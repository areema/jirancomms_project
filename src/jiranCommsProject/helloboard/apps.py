from django.apps import AppConfig


class HelloboardConfig(AppConfig):
    name = 'helloboard'
