from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User


class UserProfile(models.Model):
	user = models.OneToOneField(User)
	nickname = models.CharField(max_length=128)
	subscribe_boards = models.ManyToManyField('Viewer', related_name='subscribe_set', blank=True, null=True)
	
	def __str__(self):
		return self.nickname

class Viewer(models.Model):
	device_id = models.CharField(max_length=100)
	name = models.CharField(max_length=30)

	def __str__(self):
		return self.name

def upload_location(instance, filename):
	# id 가 생성되기 전의 시점이기 때문에 id를 이름에 사용해서는 안되겠다.
	return "%s_%s_%s" %(instance.author, instance.id, filename)

class Board(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL)
	title = models.CharField(max_length=30)
	content = models.TextField()
	image = models.ImageField(
		upload_to=upload_location,
		blank=True, 
		null=True, 
		width_field="width_field", 
		height_field="height_field")
	width_field = models.IntegerField(default=0)
	height_field = models.IntegerField(default=0)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	author = models.CharField(max_length=30)
	target = models.ForeignKey('Viewer',
			related_name="targets",
			related_query_name="target"
		)

	def __str__(self):
		return self.title

	def get_absolute_url(self, viewer_id):
		return reverse("helloboard:board_list", kwargs={'viewer_id': viewer_id})

	# class Meta:
	# 	ordering = ["created"]

class Reply(models.Model):
	content = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	author = models.CharField(max_length=30) 
	board_id = models.ForeignKey(
			'Board',
		)

	def __str__(self):
		return self.content