# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-17 12:03
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('helloboard', '0015_auto_20160417_2055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='board',
            name='user_id',
        ),
        migrations.RemoveField(
            model_name='board',
            name='user_name',
        ),
        migrations.AddField(
            model_name='board',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
