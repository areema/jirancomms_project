# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-19 09:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('helloboard', '0018_auto_20160419_1813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='board',
            name='target',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='targets', related_query_name='target', to='helloboard.Viewer'),
        ),
    ]
