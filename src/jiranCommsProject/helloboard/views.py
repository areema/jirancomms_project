from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Board, Viewer
from .forms import BoardForm
import logging
from django.contrib.auth.models import User

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



logger = logging.getLogger("views")

def board_list(request, viewer_id):
	print("[DEBUG]: viewer_id = " + str(viewer_id))
	queryset_list = Board.objects.filter(target__id=viewer_id).order_by('-created')
	
	viewer_list = Viewer.objects.all()
	logger.debug(viewer_list)
	paginator = Paginator(queryset_list, 5) # Show 25 contacts per page

	page = request.GET.get('page')
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)

	context = {
		"target": queryset_list.first().target,
		"object_list": queryset,
		"viewer_list": viewer_list
	}
	return render(request, "helloboard/board_list.html", context)


def board_detail(request, viewer_id, id=None):
	instance = get_object_or_404(Board, id=id)
	context = {
		"title": instance.title,
		"instance": instance,
	}
	return render(request, "helloboard/board_detail.html", context)

def board_create(request, viewer_id=None):
	if request.user.is_active:
		viewer = Viewer.objects.get(id=viewer_id)
		print("viewer_id = " + str(viewer.id))
		form = BoardForm(request.POST or None, request.FILES or None, initial={'target': viewer})
		if form.is_valid():
			instance = form.save(commit=False)
			instance.user = request.user
			instance.save()
			return HttpResponseRedirect(instance.get_absolute_url(viewer_id))
			
		context = {
			"is_update": False,
			"form": form,
		}
		return render(request, "helloboard/board_form.html", context)
	else:
		return HttpResponseRedirect("accounts/registraion")

def board_update(request, viewer_id, id=None):
	instance = get_object_or_404(Board, id=id)
	form = BoardForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		"is_update": True,
		"title": instance.title,
		"instance": instance,
		"form":form,
	}
	return render(request, "helloboard/board_form.html", context)

def board_delete(request, viewer_id, id=None):
	instance = get_object_or_404(Board, id=id)
	instance.delete()
	return redirect("helloboard:board_list")