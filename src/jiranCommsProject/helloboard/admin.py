from django.contrib import admin
from helloboard.models import Viewer, Board, Reply, UserProfile
# Register your models here.

class ViewerAdmin(admin.ModelAdmin):
	class Meta:
		model = Viewer
	list_display = ['pk', 'device_id', 'name']

class BoardAdmin(admin.ModelAdmin):
	class Meta:
		model = Board
	list_display = ['pk', 'target_id', 'author', 'title', 'created', 'updated']
	ordering = ["created"]

class ReplyAdmin(admin.ModelAdmin):
	class Meta:
		model = Reply
	list_display = ['pk', 'board_id', 'content']
	
class UserProfileAdmin(admin.ModelAdmin):
	class Meta:
		model = UserProfile
	list_display = ['nickname'	]

admin.site.register(Viewer, ViewerAdmin)
admin.site.register(Board, BoardAdmin)
admin.site.register(Reply, ReplyAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
