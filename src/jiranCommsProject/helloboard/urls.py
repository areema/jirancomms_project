from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^(?P<viewer_id>\d+)/$', 'helloboard.views.board_list', name='board_list'), # viewer_id
    url(r'^(?P<viewer_id>\d+)/(?P<id>\d+)/$', 'helloboard.views.board_detail', name='board_detail'), # viewer_id, board_id
    url(r'^(?P<viewer_id>\d+)/(?P<id>\d+)/edit/$', 'helloboard.views.board_update', name='board_update'), # viewer_id, board_id
    url(r'^(?P<viewer_id>\d+)/(?P<id>\d+)/delete/$', 'helloboard.views.board_delete', name='board_delete'), # viewer_id, board_id
    url(r'^(?P<viewer_id>\d+)/board_create/$', 'helloboard.views.board_create', name='board_create'),
]

