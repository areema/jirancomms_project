# Serializers define the API representation.
from rest_framework import serializers
from helloboard.models import Board

class BoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = ('title', 'content',)

