from django.contrib import admin
from helloboardRestApi.models import Test
# Register your models here.

class TestAdmin(admin.ModelAdmin):
	class Meta:
		model = Test
	list_display = ['content']

admin.site.register(Test, TestAdmin)