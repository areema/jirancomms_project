from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings

# Create your models here.
class Test(models.Model):
	content = models.CharField(max_length=100)

	def __str__(self):
		return self.content