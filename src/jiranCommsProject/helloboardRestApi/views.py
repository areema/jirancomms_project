from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from helloboard.models import Board
from .serializers import BoardSerializer

@csrf_exempt
def board_next(request):
	if request.method == 'POST':
		target_id = request.POST.get('target_id')
		board_id = request.POST.get('board_id')

		next_obj = Board.objects.filter(target__id=target_id).filter(id__gt=board_id)
		
		if next_obj:
			serializer = BoardSerializer(next_obj.first())
			return JSONResponse(serializer.data)
	
		first_obj = Board.objects.filter(target__id=target_id)[0]
		serializer = BoardSerializer(first_obj)
		return JSONResponse(serializer.data)


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)
